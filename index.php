<!DOCTYPE html>
<html>
  <head>
    <meta charset=utf-8 />
    <title>Minute Hack</title>
    <link rel="stylesheet" type="text/css" href="css/main.css" />
    <link rel="stylesheet" type="text/css" href="font/stylesheet.css" />
    <link rel="icon" type="image/gif" href="favicon.gif" />
  </head>
  <body>
    <h1>Minute Hack</h1>
    <?php 
      // options can be passed, such as:
      // https://lab.raphaelbastide.com/minutehack/?php&is_paged
      $opts = [
        "php" => 0,
        "css_extra" => 0,
        "js" => 0,
        "jquery" => 0,
        "is_paged" => 0,
        "og" => 0,
        "readme" => 0
      ];
      // parse querystring
      parse_str($_SERVER['QUERY_STRING'], $q); 
      
      // loop around options to check if any param has been passed
      foreach($q as $key => $val){
        foreach($opts as $opt_key => $opt_val){
          if($opt_key == $key){
            // check if any option is falsed (https://lab.raphaelbastide.com/minutehack/?jquery=false)
            $opts[$key] = ($val == "0" || $val == "false") ? 0 : 1;
          }
        }
      }
    ?>
    <form class="make" method="post" action="files/make.php">
      <input type="title" required name="title" class="input" placeholder="Project title">
      <div class="switch c2">
        <input type="radio" name="php" value="no" id="php_no" class="switch-input" <?= $opts['php'] == 0 ? "checked" : "" ?>>
        <label for="php_no" class="switch-label">HTML</label>
        <input type="radio" name="php" value="yes" id="php_yes" class="switch-input" <?= $opts['php'] == 1 ? "checked" : "" ?>>
        <label for="php_yes" class="switch-label">PHP</label>
      </div>
      <div class="switch c2">
        <input type="radio" name="css_extra" value="css_noextra" id="css_noextra" class="switch-input" <?= $opts['css_extra'] == 0 ? "checked" : "" ?>>
        <label for="css_noextra" class="switch-label">No CSS reset</label>
        <input type="radio" name="css_extra" value="css_reset" id="css_reset" class="switch-input" <?= $opts['css_extra'] == 1 ? "checked" : "" ?>>
        <label for="css_reset" class="switch-label">CSS reset</label>
      </div>
      <div class="switch c3">
        <input type="radio" name="js" value="no_js" id="js" class="switch-input" <?= ($opts['js'] == 0 && $opts['jquery'] == 0) ? "checked" : "" ?>>
        <label for="js" class="switch-label">No JS</label>
        <input type="radio" name="js" value="plain_js" id="plain_js" class="switch-input" <?= ($opts['js'] == 1  && $opts['jquery'] == 0) ? "checked" : "" ?>>
        <label for="plain_js" class="switch-label">JS</label>
        <input type="radio" name="js" value="jquery" id="jquery" class="switch-input" <?= $opts['jquery'] == 1 ? "checked" : "" ?>>
        <label for="jquery" class="switch-label">JS + jQuery</label>
      </div>
      <div class="switch c2">
        <input type="radio" name="is_paged" value="not_paged" id="not_paged" class="switch-input" <?= $opts['is_paged'] == 0 ? "checked" : "" ?>>
        <label for="not_paged" class="switch-label">Web only</label>
        <input type="radio" name="is_paged" value="paged" id="paged" class="switch-input" <?= $opts['is_paged'] == 1 ? "checked" : "" ?>>
        <label for="paged" class="switch-label">Web to print</label>
      </div>
      <div class="switch c2">
        <input type="radio" name="og" value="no" id="og_no" class="switch-input" <?= $opts['og'] == 0 ? "checked" : "" ?>>
        <label for="og_no" class="switch-label">Minimal HTML header</label>
        <input type="radio" name="og" value="yes" id="og_yes" class="switch-input" <?= $opts['og'] == 1 ? "checked" : "" ?>>
        <label for="og_yes" class="switch-label">Social HTML header</label>
      </div>
      <div class="switch c2">
        <input type="radio" name="readme" value="no" id="readme_no" class="switch-input" <?= $opts['readme'] == 0 ? "checked" : "" ?>>
        <label for="readme_no" class="switch-label">No README.md</label>
        <input type="radio" name="readme" value="yes" id="readme_yes" class="switch-input" <?= $opts['readme'] == 1 ? "checked" : "" ?>>
        <label for="readme_yes" class="switch-label">README.md</label>
      </div>
      <input type="submit" value="* * Generate * *" class="button swing">
    </form>
    <p><a href="https://gitlab.com/raphaelbastide/Minute-Hack">Minute Hack is open source</a></p>
  </body>
</html>
